﻿using System;
using UnityEngine;

[Serializable]
public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public Sound[] Sounds;

    // Use this for initialization
    void Awake()
    {
        // Simple Singleton pattern
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        foreach (var sound in Sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;
        }
    }

    public void Play(string soundName)
    {
        var s = Array.Find(Sounds, sound => sound.name == soundName);
        if (!CheckThatSoundIsFound(s, soundName)) return;
        s.source.Play();
    }
    public void PlayOneShot(string soundName)
    {
        var s = Array.Find(Sounds, sound => sound.name == soundName);
        if (!CheckThatSoundIsFound(s, soundName)) return;
        s.source.Stop();
    }
    public void Stop(string soundName)
    {
        var s = Array.Find(Sounds, sound => sound.name == soundName);
        if (!CheckThatSoundIsFound(s, soundName)) return;
        s.source.Stop();
    }


    private static bool CheckThatSoundIsFound(Sound s, string name)
    {
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found");
            return false;
        }
        else
        {
            return true;
        }
    }
}


