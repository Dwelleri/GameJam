﻿using UnityEngine.Audio;
using UnityEngine;

public class Sound
{
    public string name;
    public bool loop;

    [HideInInspector]
    public AudioSource source;
    public AudioClip clip;

    [Range(0f, 1f)] public float volume;
    [Range(.1f, 3f)] public float pitch;
}